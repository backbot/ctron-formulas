Gem::Specification.new do |spec|
  spec.name          = 'ctron-formulas'
  spec.version       = '0.5.0'
  spec.authors       = ['Daniel Schlegel']
  spec.email         = ['daniel.schlegel@gmx.net']
  spec.summary       = 'Ctron base formulas.'
  spec.description   = 'Opensource Ctron formulas.'
  spec.homepage      = 'https://www.cloudgear.net'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.0'

  spec.add_runtime_dependency 'ctron', '~> 0.5.0'

  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.0'
end
