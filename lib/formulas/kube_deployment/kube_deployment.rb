class KubeDeployment < Formula::Base
  required :name
  required :image
  required :container_port
  optional :data_dir, default: "/data"
  optional :namespace, default: Proc.new { |f| 'default' }
  optional :port, default: 80 # musst be according to domain. if domain.com:port then use this port also here
  optional :domain
  optional :env
  optional :node
  optional :volumes
  optional :http_user
  optional :http_passwd
  optional :deploy_ones, default: false
  optional :image_pull_policy, default: "Always"
  optional :args

  id :name

  def before
    kube_deploy params
    kube_svc params
  end

end
