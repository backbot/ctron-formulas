class HelmFetch < Formula::Base
  required :chart
  required :temp_dir
  id :chart

  def init
    params.chart = params.chart
  end
end
