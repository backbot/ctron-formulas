class KubectlExec < Formula::Base

  required :pod
  required :command
  optional :retry, default: true
  optional :ignore_errors, default: false
  optional :namespace, Proc.new { |f| ENV['CTRON_NAMESPACE'] || 'default' }
  optional :container_name # container inside pod if more than one container
  id { |f| "#{f.params.namespace}-#{f.params.pod}-#{f.params.command.gsub(' ', '-')}" }

  def init
    params.retry_command = params.retry
  end
end
