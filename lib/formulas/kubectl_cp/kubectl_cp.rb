class KubectlCp < Formula::Base

  required :pod
  required :source
  optional :destination, default: '/tmp'
  optional :namespace, Proc.new { |f| ENV['CTRON_NAMESPACE'] || 'default' }
  optional :container_name # container inside pod if more than one container

  id { |f| puts f.params.pod; puts f.params.inspect; "#{f.params.namespace}-#{f.params.pod}-#{f.params.source}" }

end
