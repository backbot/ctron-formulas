class KubeSvc < Formula::Base

  include Lib::Base

  required :container_port
  optional :domain # e.g. domain.com:port
  optional :port, default: 80 # musst be according to domain. if domain.com:port then use this port also here
  optional :namespace, default: Proc.new {|f| ENV['CTRON_NAMESPACE'] || 'default' }
  optional :http_user
  optional :http_passwd
  optional :deploy_ones, default: true
  optional :selector_id

  def deploy_type
    "svc"
  end

end
