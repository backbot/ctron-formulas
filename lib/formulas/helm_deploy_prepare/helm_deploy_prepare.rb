class HelmDeployPrepare < Formula::Base
  # chart
  required :chart # e.g. "chart_name" not "stable/chart_name" stable will be added automatically.

  # kubernetes
  optional :server

  optional :token
  # or
  optional :user
  optional :password

  # additional opts
  optional :service_account
  optional :temp_dir, default: "tmp/src"

  id :chart

  def after
    config_file = File.join(params.temp_dir, '.kube/config')
    kube_config server: params.server,
                token: params.token,
                user: params.user,
                password: params.password,
                kube_config_path: config_file

    helm_init config_file: config_file,
              service_account: params.service_account

    helm_fetch chart: params.chart,
               temp_dir: params.temp_dir
  end
end
