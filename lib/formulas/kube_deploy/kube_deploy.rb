class KubeDeploy < Formula::Base

  include Lib::Base

  required :id
  required :image
  optional :container_port
  optional :env
  optional :data_dir, default: "/data"
  optional :namespace, default: Proc.new {|f| ENV['CTRON_NAMESPACE'] || 'default' }
  optional :node
  optional :volumes
  optional :deploy_ones, default: false
  optional :image_pull_policy, default: "Always"
  optional :args

  def deploy_type
    "deploy"
  end

end
