class HelmInit < Formula::Base
  optional :config_file, default: ".kube/config"
  optional :client_only, default: true
  optional :service_account

  id :config_file
end
