class HelmDeployRun < Formula::Base
  # chart
  required :chart # e.g. "chart_name" not "stable/chart_name" stable will be added automatically.
  optional :version

  # deployment config
  required :release
  optional :namespace, default: 'default'
  optional :remove, default: false # remove deployed app
  optional :remove_namespace, default: false # namespace name or false. this ns will be completely removed.

  # kubernetes
  optional :config_file

  # ingress # TODO (ds): make optional
  required :name
  required :secret_name
  required :service_name
  required :service_port
  required :domain

  # additional opts
  optional :temp_dir, default: "tmp/src"

  id :chart

  def after
    config_file = params.config_file || File.join(params.temp_dir, '.kube/config')
    if params.remove && params.remove.to_s == "true"
      helm_delete release: params.release,
                  config_file: config_file,
                  remove_namespace: params.remove_namespace ? params.remove_namespace : false # this will remove namespace completely
      # TODO (ds): remove ingress
    else
      helm_upgrade chart: File.join(params.temp_dir, params.chart), # TODO (ds): chart is e.g. "stable/moodle" but should be "moodle"
                   namespace: params.namespace,
                   version: params.version,
                   release: params.release,
                   config_file: config_file,
                   opts: params.opts
    end

    if params.name && params.domain && params.service_name && params.secret_name
      kube_ingress name: params.name,
                   service_name: params.service_name,
                   service_port: params.service_port,
                   secret_name: params.secret_name,
                   namespace: params.namespace,
                   domain: params.domain,
                   remove: params.remove
    end
  end
end
