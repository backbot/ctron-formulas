module Formula
  class KubeIngress < Base
    required :name
    required :service_name
    required :service_port
    required :secret_name
    required :namespace
    required :domain
    optional :config_file, default: "tmp/src/.kube/config"
    optional :remove, default: false

    id :name

    def init
      params.destination = "tmp/src/ingress.yaml"
    end

    def before
      file source: "ingress.yaml",
           destination: params.destination
    end
  end
end
