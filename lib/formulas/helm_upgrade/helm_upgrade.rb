class HelmUpgrade < Formula::Base
  required :chart # e.g. "stable/moodle or directory"
  required :release # e.g. "develop"
  required :namespace
  optional :config_file, default: ".kube/config"
  optional :version

  id :chart

  def init
    opts = []
    opts << "--namespace #{params.namespace}"
    opts << "--version #{params.version}" if params.version && !params.version.empty?
    params.opts = opts.join(" ")
  end

end
