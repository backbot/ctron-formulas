class KubectlDeleteNs < Formula::Base

  required :config
  optional :namespace, default: 'default'
  id  {|f| "#{f.params.namespace}-#{f.params.config}"}

  def init
    params.name = File.basename(params.config).split('.').first
    params.deploy_type = File.basename(params.config).split('.')[-2]
  end

end
