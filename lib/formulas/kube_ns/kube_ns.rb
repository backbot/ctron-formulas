class KubeNs < Formula::Base

  include Lib::Base

  required :id
  required :namespace
  optional :data_dir, default: "/data"
  optional :kube_config_path

  def deploy_type
    "ns"
  end

end
