module Formula
  class Remove < Base
    required :target
    optional :recursive, default: false
    optional :force, default: false
    id :target

  end
end
