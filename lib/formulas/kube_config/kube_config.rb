class KubeConfig < Formula::Base
  required :server

  # either you should give token or username with password. # TODO (ds): conditional validators
  optional :token

  optional :user
  optional :password

  optional :kube_config_path, default: "tmp/.kube/config"
  id :server

  def after
    if params.token
      file source: "config_token", destination: params.kube_config_path
    elsif params.user && params.password
      file source: "config_user", destination: params.kube_config_path
    else
      raise "Please provide user and password or token."
    end
  end
end
