module Formula
  class File < Base
    required :destination
    required :source
    optional :mode
    optional :owner
    optional :group
    id :destination

    def init
      params[:source] = ::File.join(parent_files_dir, params[:source])
    end
  end
end
