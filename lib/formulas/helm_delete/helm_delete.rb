class HelmDelete < Formula::Base
  required :release # e.g. "app_name"
  optional :config_file, default: ".kube/config"
  optional :remove_namespace, default: false # name of namespace to remove

  id :release
end
