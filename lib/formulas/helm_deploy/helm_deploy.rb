class HelmDeploy < Formula::Base
  # chart
  required :chart
  optional :version

  # deployment config
  required :release
  optional :namespace
  optional :remove, default: false # remove deployed app
  optional :remove_namespace, default: false # namespace name or false. this ns will be completely removed.

  # kubernetes
  optional :server
  optional :token

  id :chart

  def after
    # TODO (ds): combine prepare and run formulas
  end
end
