require 'base'
require 'ctron'

# Load formulas
formulas_path = File.expand_path('../formulas', __FILE__)
Ctron.catalog.load_formulas([formulas_path])
