module Lib
  module Base

    def init
      params.volumes.each do |v|
        # automatically choose host_path.
        v.host_path = File.join(volumes_folder, v.name) unless v.host_path
        # if ENV['CTRON_REMOVE_data_dir'] && remove?
        #   puts "WARN: Will remove all data in '#{volumes_folder}' now!"
        #   remove Hashie::Mash.new(target: volumes_folder, recursive: true, force: true)
        # end

      end if params.volumes
    end

    def before
      file destination: file_path, source: "config.yml"
    end

    def after
      if remove?
        # remove all deployed services and controllers.
        kubectl_delete Hashie::Mash.new(config: file_path,
                                        namespace: params.namespace,
                                        kube_config_path: params.kube_config_path)
      else
        kubectl_apply Hashie::Mash.new(config: file_path,
                                       namespace: params.namespace,
                                       deploy_ones: params.deploy_ones,
                                       kube_config_path: params.kube_config_path)
      end
    end

    private

    def file_path
      File.join(config_folder, params.namespace, file_name)
    end

    def file_name
      "#{params.id}.#{deploy_type}.yml"
    end

    def config_folder
      "tmp/ctron/config"
    end

    def data_dir
       params.data_dir
    end

    def volumes_folder
      File.join(data_dir, params[:id])
    end

    def deploy_type
      raise Exception.new "Please specify deploy_type method inside formula."
    end

    def remove?
      ENV['CTRON_REMOVE'] && ENV['CTRON_REMOVE'].to_s == 'true'
    end

  end
end
