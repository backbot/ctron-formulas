# Ctron Docker Formulas

Ctron formulas for remote kubernetes configs.

## Installation

Add this line to your Ctron based project's Gemfile:

```ruby
gem 'ctron-formulas'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ctron-formulas

## Usage

# env variables

CTRON_REMOVE
CTRON_DATA_FOLDER
CTRON_CONFIG_FOLDER

